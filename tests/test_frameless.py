import sys

import qdarktheme

from PyQt5 import QtCore
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication, QMenu
from qframelesswindow import FramelessMainWindow, TitleBar, StandardTitleBar


class CustomTitleBar(TitleBar):
    """ Custom title bar """

    def __init__(self, parent=None):
        super().__init__(parent)

        # customize the style of title bar button
        self.minBtn.setHoverColor(QtCore.Qt.white)
        self.minBtn.setHoverBackgroundColor(QColor(100, 100, 182))
        self.minBtn.setPressedColor(QtCore.Qt.white)
        self.minBtn.setPressedBackgroundColor(QColor(54, 57, 65))

        # use qss to customize title bar button
        self.maxBtn.setStyleSheet("""
            TitleBarButton {
                qproperty-normalColor: black;
                qproperty-normalBackgroundColor: transparent;
                qproperty-hoverColor: white;
                qproperty-hoverBackgroundColor: rgb(0, 100, 182);
                qproperty-pressedColor: white;
                qproperty-pressedBackgroundColor: rgb(54, 57, 65);
            }
        """)

        



class Window(FramelessMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("PyQt-Frameless-Window")
        self.setTitleBar(CustomTitleBar(self))

        self.setWindowTitle("PyQt-Frameless-Window")

        # don't forget to put the title bar at the top
        self.titleBar.raise_()


if __name__ == '__main__':
    qdarktheme.enable_hi_dpi()
    app = QApplication(sys.argv)
    # qdarktheme.setup_theme(theme="dark")
    demo = Window()
    demo.show()
    sys.exit(app.exec_())