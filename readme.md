# TGMS made easy

This is a really niche project, meant to save time while processing TGMS experiment data at Djamila's lab.

**Latest changes**

- Creation of the thing

## Getting started

Just smash

```bash
pip install -r ./requirements.txt
python ./tgms_made_easy.py
```


## Roadmap

- **Parse data files**
- **Plot *vs* temperature/time**
- Increase export possibilities
- Make the plot more customizable
  - Pen parameters for each channel
  - Axes renaming
  - Derivatives of TG ?
  - Shift + Click as Vincent did
- Preferred palettes in settings