# TGMS made easy

This piece of software is designed to import TGA (ThermoGravimetric Analysis) and MS (Mass Spectrometry) data, and present them as a TGMS plot.

## Getting started

### Importing

1. Import MS data (in the left toolbar or `Ctrl+M`). Follow the dialog, and make sure the lines and columns are well recognized in the table preview.
2. Click `OK`. The MS channels should appear in the left pane, in  a section  called `Curves to plot`.
3. Do the same for TG data (toolbar or `Ctrl+T`).
4. You can toggle the curves you want to plot with the checkboxes, and resize the view as you like. If you are lost, the little 'A' symbol on the bottom left of the graph will autoscale the window.

### Exporting

With the `Export` button in the toolbar (`Ctrl+Shift+S`), you can export the data as a single CSV table.

### Opening and saving projects

The software can save projects as JSON data files. `Save` (`Ctrl+S`) will save the state into the JSON file (your channels, data, and metadata will be saved), `Open` (`Ctrl+O`) will open it.



