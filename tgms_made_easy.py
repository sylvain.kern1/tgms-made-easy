import sys
import codecs
import pickle
import json
import base64
import webbrowser

import numpy as np
import quantities as pq
import pyqtgraph as pg
import pandas as pd
import pyqtgraph.parametertree.parameterTypes as pTypes

from pyqtgraph.parametertree import Parameter, ParameterTree
from PyQt6 import QtCore, QtWidgets, QtGui
from seaborn import color_palette
from pyqtgraph.dockarea import Dock
from pyqtgraph.dockarea import DockArea
from qdarktheme import _style_loader
from io import StringIO
from functools import partial

from animated_toggle import AnimatedToggle

import qdarktheme

import tgms_processing


SETTINGS_FILE = "settings.json"
VERSION = "0.1"

class settings_dialog(QtWidgets.QDialog):

    def __init__(self, win):
        
        super().__init__(windowTitle="Settings")

        self.win = win

        self.load_from_settings()

        self.ui_setup() 

        self.execute()

    def ui_setup(self):
        layout = QtWidgets.QVBoxLayout()

        open_settings = QtWidgets.QPushButton()
        open_settings.setText("change in settings.json")
        layout.addWidget(open_settings)

        frame1 = QtWidgets.QGroupBox()
        frame1.setTitle("Appearance")
        frame2 = QtWidgets.QGroupBox()
        frame2.setTitle("Save && export")
        frame3 = QtWidgets.QGroupBox()
        frame3.setTitle("...")
        layout.addWidget(frame1)
        layout.addWidget(frame2)
        layout.addWidget(frame3)

        frame1layout = QtWidgets.QVBoxLayout()
        frame1.setLayout(frame1layout)
        
        self.darklight = AnimatedToggle()
        if self.settings_dict["Appearance"]["Dark mode"]:
            self.darklight.setCheckState(QtCore.Qt.CheckState.Checked)
        self.darklight.setFixedSize(self.darklight.sizeHint())
        darklightlayout = QtWidgets.QHBoxLayout()
        self.darklight.stateChanged.connect(self.darklight_changed)
        darklightlayout.addWidget(QtWidgets.QLabel("Dark mode"))
        darklightlayout.addWidget(self.darklight)

        frame1layout.addLayout(darklightlayout)

        buttonbox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.StandardButton.Ok)
        buttonbox.accepted.connect(self.save_settings)
        buttonbox.accepted.connect(self.accept)

        layout.addWidget(buttonbox)

        self.setLayout(layout)

    def darklight_changed(self):
        if self.darklight.isChecked():
            qdarktheme.setup_theme(theme="dark")
            self.win.tgms_plot.setBackground(None)
            self.settings_dict["Appearance"]["Dark mode"] = True
        else:
            qdarktheme.setup_theme(theme="light")
            self.win.tgms_plot.setBackground(None)
            self.settings_dict["Appearance"]["Dark mode"] = False        
    
    def execute(self):
        # dialog execution
        # self.setLayout(self.dialoglayout)
        self.resize(300,600)
        self.exec()

    def load_from_settings(self):
        try:
            with open(SETTINGS_FILE, "r") as handle:
                self.settings_dict = json.load(handle)
        except FileNotFoundError:
            if _style_loader._detect_system_theme("") == "dark":
                d = True
            else:
                d = False
            self.settings_dict = {
                "Appearance": {
                    "Dark mode": d
                }
            }
    
    def save_settings(self):
        
        with open(SETTINGS_FILE, "w") as handle:
            json.dump(self.settings_dict, handle)


class import_dialog(QtWidgets.QDialog):

    def __init__(self, msortg):

        super().__init__(windowTitle="Importing "+msortg+" data")

        self.msortg = msortg

        self.ui_setup()

        # loading data
        self.load(loadfile=True)
        
        self.execute()

    def execute(self):
        # dialog execution
        self.setLayout(self.dialoglayout)
        self.resize(1100,600)
        self.exec()

    def ui_setup(self):
        
        # change the file while still in window
        self.filelabel = QtWidgets.QLineEdit()
        self.filelabel.setReadOnly(True)
        browse = QtWidgets.QPushButton(text="…")
        
        toplayout = QtWidgets.QHBoxLayout()
        toplayout.addWidget(self.filelabel)
        toplayout.addWidget(browse)

        # bottom standard buttons
        buttonbox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.StandardButton.Cancel | QtWidgets.QDialogButtonBox.StandardButton.Ok)
        buttonbox.accepted.connect(self.accept)
        buttonbox.rejected.connect(self.reject)

        # textfile preview area
        self.textBoxLayout = QtWidgets.QHBoxLayout()
        # self.textWidget = LineNumberWidget()
        # self.lineNumberWidget = LineNumberWidget(self.textWidget)
        # self.textWidget.setFontFamily("Consolas")
        self.textWidget = TextPreviewWidget()

        # table preview area
        self.tableWidget = QtWidgets.QTableView()
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollMode.ScrollPerPixel)
        self.tableWidget.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollMode.ScrollPerPixel)

        # docks text / table
        docks = DockArea()
        textDock = Dock("Text preview", hideTitle=True)
        tableDock = Dock("Table view", hideTitle=True)   
        textDock.setOrientation('horizontal')
        textlayout = QtWidgets.QVBoxLayout()
        textBoxLayout = QtWidgets.QHBoxLayout()
        textBoxWidget = QtWidgets.QWidget()
        # textBoxLayout.addWidget(self.lineNumberWidget)
        # textBoxWidget.setLayout(textBoxLayout)
        # textBoxLayout.addWidget(self.textWidget)
        # textBoxLayout.addLayout(textlayout)
        textDock.addWidget(self.textWidget) 
        tableDock.addWidget(self.tableWidget)
        docks.addDock(textDock)
        docks.addDock(tableDock)

        # import options layout
        importoptlayout = QtWidgets.QVBoxLayout()
        frame = QtWidgets.QGroupBox()
        frame.setTitle("Import options")
        frame.setMaximumWidth(200)
        framelayout = QtWidgets.QVBoxLayout()
        framelayout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
        frame.setLayout(framelayout)
        importoptlayout.addWidget(frame)

        # import options
            ## skip rows
        rowstoskip_title = QtWidgets.QLabel()
        rowstoskip_title.setText("Skip rows")
        rowstoskip_title.adjustSize()
        self.rowstoskip_edit = QtWidgets.QSpinBox()
        self.rowstoskip_edit.adjustSize()

        rowstoskip_layout = QtWidgets.QHBoxLayout()
        rowstoskip_layout.addWidget(rowstoskip_title)
        rowstoskip_layout.addWidget(self.rowstoskip_edit)

        framelayout.addLayout(rowstoskip_layout)

            ## separator
        separator_title = QtWidgets.QLabel()
        separator_title.setText("Separator")
        separator_title.adjustSize()
        self.separator_edit = QtWidgets.QComboBox()
        self.separator_edit.addItems([',',';','TAB'])
        self.separator_edit.adjustSize()

        separator_layout = QtWidgets.QHBoxLayout()
        separator_layout.addWidget(separator_title)
        separator_layout.addWidget(self.separator_edit)

        framelayout.addLayout(separator_layout)

            ## decimal
        decimal_title = QtWidgets.QLabel()
        decimal_title.setText("Decimal")
        decimal_title.adjustSize()
        self.decimal_edit = QtWidgets.QComboBox()
        self.decimal_edit.addItems(['.',','])
        self.decimal_edit.adjustSize()

        decimal_layout = QtWidgets.QHBoxLayout()
        decimal_layout.addWidget(decimal_title)
        decimal_layout.addWidget(self.decimal_edit)

        framelayout.addLayout(decimal_layout)

        reload_button = QtWidgets.QPushButton(text="Apply")
        framelayout.addWidget(reload_button)

        # window layout
        midlayout = QtWidgets.QHBoxLayout()
        midlayout.addLayout(importoptlayout)
        midlayout.addWidget(docks)
        self.dialoglayout = QtWidgets.QVBoxLayout()
        self.dialoglayout.addLayout(toplayout)
        self.dialoglayout.addLayout(midlayout)
        self.dialoglayout.addWidget(buttonbox)

        # button actions
        reload_button.clicked.connect(lambda: self.load(loadfile=False))
        browse.clicked.connect(lambda: self.load(loadfile=True))

    def load(self, loadfile, skiprows=0, sep=',', decimal='.'):

        self.skiprows = self.rowstoskip_edit.value()
        if self.separator_edit.currentText() == "TAB":
            self.sep = "\t"
        else:
            self.sep = self.separator_edit.currentText()
        self.decimal = self.decimal_edit.currentText()
        
        if loadfile:
            self.filename = self.open_csv_file(msortg=self.msortg)
            self.text = self.get_contents(self.filename)
            

        print("imported "+self.filename)

        # print(self.text)
        
        # with open(self.filename, 'r') as f:
        try:
            data = pd.read_csv(StringIO(self.text), sep=self.sep, decimal=self.decimal, skiprows=self.skiprows, nrows=100, skipinitialspace = True, )
            headers = list(data)
        except pd.errors.ParserError or TypeError:
            data = pd.DataFrame([[]])
            headers = []
            print("could not parse CSV with current settings")

        self.model = PandasModel(data)
        self.data = data

        self.tableWidget.setModel(self.model)
        self.textWidget.setPlainText(self.text)
        self.filelabel.setText(self.filename)

    def get_contents(self, filename):
        # BLOCKSIZE = 1048576 # or some other, desired size in bytes
        # newfilename = ''.join(filename.split(".")[:-1])+'_utf8.'+filename.split(".")[-1]
        with codecs.open(filename, "rb", errors="ignore") as sourceFile:
            contents = sourceFile.read()
            return contents.decode(errors="ignore")



    def open_csv_file(self, msortg):
        filename, filter = QtWidgets.QFileDialog.getOpenFileName(
            caption="Open " + msortg + " data",
            filter="CSV files (*.csv *.txt);;All files(*)",
        )
        return filename
    
    def accepted(self):
        return super().accepted()
    
    def rejected(self):
        return super().rejected()


class Window(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.tgms = tgms_processing.TGMS_data()
        self.ui_setup()

    def ui_setup(self):

        # TITLE
        self.setWindowTitle("TGMS made easy")

        # TOOLBAR
        toolbar = QtWidgets.QToolBar("Toolbar")
        toolbar.setOrientation(QtCore.Qt.Orientation.Vertical)
        self.addToolBar(QtCore.Qt.ToolBarArea.LeftToolBarArea, toolbar)

        import_ms = QtGui.QAction("MS", self)
        import_ms.setStatusTip("This is your button")
        import_ms.triggered.connect(self.import_ms)
        toolbar.addAction(import_ms)

        import_tg = QtGui.QAction("TG", self)
        import_tg.setStatusTip("This is your button")
        import_tg.triggered.connect(self.import_tg)
        toolbar.addAction(import_tg)

        toolbar.addSeparator()

        log = QtGui.QAction("log", self)
        log.setStatusTip("This is your button")
        log.setShortcut(QtGui.QKeySequence("Alt+Shift+L"))
        log.setCheckable(True)
        log.triggered.connect(self.make_log(log.isChecked))
        toolbar.addAction(log)

        tT = QtGui.QAction("t ⟷ T°", self)
        tT.setStatusTip("This is your button")
        tT.setShortcut(QtGui.QKeySequence("Alt+Shift+T"))
        tT.setCheckable(True)
        tT.triggered.connect(self.make_tvsT(tT.isChecked()))
        toolbar.addAction(tT)

        spacer = QtWidgets.QWidget()
        spacer.setSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Expanding)
        toolbar.addWidget(spacer)

        open = QtGui.QAction("Open", self)
        open.setStatusTip("This is your button")
        open.triggered.connect(self.open)
        toolbar.addAction(open)

        save = QtGui.QAction("Save", self)
        save.setStatusTip("This is your button")
        save.triggered.connect(self.save)
        toolbar.addAction(save)

        xport = QtGui.QAction("Export", self)
        xport.setStatusTip("This is your button")
        xport.triggered.connect(self.export)
        toolbar.addAction(xport)

        toolbar.addSeparator()

        settings = QtGui.QAction("Settings", self)
        settings.setStatusTip("This is your button")
        settings.triggered.connect(self.settings)
        toolbar.addAction(settings)   

        # MENUBAR
        menuBar = self.menuBar()
        
        fileMenu = QtWidgets.QMenu("&File", self)

        import_ms_action = QtGui.QAction("Import MS data", self)
        import_ms_action.setShortcut(QtGui.QKeySequence("Ctrl+M"))
        import_ms_action.setStatusTip("This is your button")
        import_ms_action.triggered.connect(self.import_ms)
        fileMenu.addAction(import_ms_action)

        import_tg_action = QtGui.QAction("Import TG data", self)
        import_tg_action.setShortcut(QtGui.QKeySequence("Ctrl+T"))
        import_tg_action.setStatusTip("This is your button")
        import_tg_action.triggered.connect(self.import_ms)
        fileMenu.addAction(import_tg_action)

        fileMenu.addSeparator()

        open_action = QtGui.QAction("Open", self)
        open_action.setShortcut(QtGui.QKeySequence("Ctrl+O"))
        open_action.setStatusTip("This is your button")
        open_action.triggered.connect(self.open)
        fileMenu.addAction(open_action)

        save_action = QtGui.QAction("Save", self)
        save_action.setShortcut(QtGui.QKeySequence("Ctrl+S"))
        save_action.setStatusTip("This is your button")
        save_action.triggered.connect(self.save)
        fileMenu.addAction(save_action)

        export_action = QtGui.QAction("Export as CSV", self)
        export_action.setShortcut(QtGui.QKeySequence("Ctrl+Shift+S"))
        export_action.setStatusTip("This is your button")
        export_action.triggered.connect(self.export)
        fileMenu.addAction(export_action)

        fileMenu.addSeparator()

        settings_action = QtGui.QAction("Settings", self)
        settings_action.setShortcut(QtGui.QKeySequence("Ctrl+,"))
        settings_action.setStatusTip("This is your button")
        settings_action.triggered.connect(self.settings)
        fileMenu.addAction(settings_action)

        fileMenu.addSeparator()

        about_action = QtGui.QAction("About...", self)
        about_action.setStatusTip("This is your button")
        about_action.triggered.connect(self.about)
        fileMenu.addAction(about_action)

        menuBar.addMenu(fileMenu)

        viewMenu = QtWidgets.QMenu("&View", self)
        viewMenu.addAction(toolbar.toggleViewAction())
        
        viewMenu.addSeparator()

        viewMenu.addAction(log)
        viewMenu.addAction(tT)

        menuBar.addMenu(viewMenu)

        def open_help():
            webbrowser.open("https://gitlab.univ-lille.fr/sylvain.kern1/tgms-made-easy/-/blob/master/doc/users_guide.md")

        help_action = QtGui.QAction("&Help", self)
        help_action.setShortcut(QtGui.QKeySequence("Ctrl+H"))
        help_action.triggered.connect(open_help)

        menuBar.addAction(help_action)

        # DOCKS
        docks = DockArea()
        parametersDock = Dock("Parameters", size=(500, 800))
        plottingDock = Dock("TGMS plot", size=(1000, 800))
        self.tgms_plot = TGMSplot()
        self.X = "Temperature [°C]"
        plottingDock.addWidget(self.tgms_plot)
        self.setCentralWidget(docks)

        # PARAMETER TREE
        self.tree = ParameterTree(showHeader=False)
        self.tree.sigItemCheckStateChanged.connect(self.update_tgms_plot)

        ## experimental parameters
        exp = Experimental_parameters()
        self.generate_experimental_parameters(exp.dict)
        self.tree.setParameters(self.experimental_parameters)

        parametersDock.addWidget(self.tree) 
        
        docks.addDock(parametersDock, 'left')
        docks.addDock(plottingDock, 'right')

        # plotting dock
        # self.setup_plot()

    def save_settings(self):
        with open("settings.json", "w") as handle:
            handle.write(self.settings_dict)

    def generate_experimental_parameters(self, dict):
        self.experimental_parameters = Parameter.create(
            name = "Experimental parameters",
            type = "group",
            children = [
                self.get_qt_parameter(param) for param in (dict.values())
            ]
        )
        self.experimental_parameters.addChild(pTypes.TextParameter(name="Notes"))


    def generate_plotting_parameters(self):

        self.plotting_parameters = Parameter.create(
            name = "Curves to plot",
            type = "group",
            children = []
        )
        self.tree.setParameters(self.experimental_parameters)

        children = []
        if self.tgms.check_for_ms():
            children.append(
                Parameter.create(
                    name = "Mass spec",
                    type = "group",
                    children = [
                        {"name": elt,
                        "type": "bool",
                        "value": False,
                        "default": None,
                        "renamable": True
                        } for elt in self.tgms.ms_channels
                    ]
                )
            )

        if self.tgms.check_for_tg():
            children.append(
                Parameter.create(
                    name = "TGA",
                    type = "group",
                    children = [
                        {"name": elt,
                        "type": "bool",
                        "value": False,
                        "default": None,
                        "renamable": True
                        } for elt in self.tgms.tg_channels
                    ]
                )
            )
            
        self.plotting_parameters.addChildren(children)
        self.tree.addParameters(self.plotting_parameters)
        self.plotting_parameters.sigTreeStateChanged.connect(self.update_tgms_plot)
        self.plotting_parameters_names = {}
        for group in self.plotting_parameters.children():
            for child in group.children():
                self.plotting_parameters_names[id(child)] = group.name(), child.name()
                child.sigTreeStateChanged.connect(self.handle_renaming)    

    def handle_renaming(self, param, changes):
        for param, change, data in changes:
            address = id(param)
            newname = param.name()
            if change == 'name':
                group, oldname = self.plotting_parameters_names[address]
                if group == "Mass spec":
                    self.tgms.ms_data = self.tgms.ms_data.rename(
                        columns=
                        {
                            oldname:
                            newname,
                        }
                    )
                    self.tgms.ms_channels[self.tgms.ms_channels.index(oldname)] = newname
                elif group == "TGA":
                    self.tgms.tg_data = self.tgms.tg_data.rename(
                        columns=
                        {
                            oldname:
                            newname,
                        }
                    )
                    self.tgms.tg_channels[self.tgms.tg_channels.index(oldname)] = newname
                self.plotting_parameters_names[address] = group, newname
                self.update_tgms_plot()

    def get_qt_parameter(self, tgms_parameter):
        try:
            suffix = tgms_parameter["value"].dimensionality.string
        except AttributeError:
            suffix = None
            typ = tgms_parameter["value"].__class__.__name__
            value = tgms_parameter["value"]
            siprefix = False
        else:
            typ = "float"
            value = float(tgms_parameter["value"].magnitude)
            siprefix = False
            
        return {
            "name": tgms_parameter["name"],
            "type": typ,
            "value": value,
            "suffix": suffix,
            "siPrefix": siprefix,
            "default": None
        }
    
    # ACTIONS

    def import_ms(self):
        dialog = import_dialog(msortg="MS")
        self.tgms.import_ms_data(
            content = dialog.text,
            rows_to_skip=dialog.skiprows,
            sep=dialog.sep,
            decimal=dialog.decimal)
        print(self.tgms.ms_data)
        self.generate_plotting_parameters()
        if self.tgms.check_for_tg():
            self.tgms.interp_temperature()
            print(self.tgms.ms_data)
            print(self.tgms.tg_data)

    def import_tg(self):
        dialog = import_dialog(msortg="TG")
        self.tgms.import_tg_data(
            content = dialog.text,
            rows_to_skip=dialog.skiprows,
            sep=dialog.sep,
            decimal=dialog.decimal)
        print(self.tgms.tg_data)
        self.generate_plotting_parameters()
        if self.tgms.check_for_ms():
            self.tgms.interp_temperature()
            print(self.tgms.ms_data)
            print(self.tgms.tg_data)
        
    def update_tgms_plot(self):

        self.tgms_plot.clf()

        print()
        enabled_ms_channels = []
        enabled_tg_channels = []

        try:
            ms_params = self.plotting_parameters.child("Mass spec")
        except KeyError:
            pass
        else:
            for parameter in ms_params.children():
                if parameter.value():
                    enabled_ms_channels.append(parameter.name())
            print(enabled_ms_channels)

            for channel in enabled_ms_channels:
                self.tgms_plot.add_ms_plot(
                    datax=np.array(self.tgms.ms_data[self.X]),
                    datay=np.array(self.tgms.ms_data[channel]),
                    name=channel
                )
        
        try:
            tg_params = self.plotting_parameters.child("TGA")
        except KeyError:
            pass
        else:
            for parameter in tg_params.children():
                if parameter.value():
                    enabled_tg_channels.append(parameter.name())
            print(enabled_tg_channels)

            for channel in enabled_tg_channels:
                self.tgms_plot.add_tg_plot(
                    datax=np.array(self.tgms.tg_data[self.X]),
                    datay=np.array(self.tgms.tg_data[channel]),
                    name=channel
                )

        if self.tgms_plot.count_ms_curves() + self.tgms_plot.count_tg_curves() >= 10:
            self.tgms_plot.legend.setColumnCount(2)
        else:
            self.tgms_plot.legend.setColumnCount(1)
                
    def make_tvsT(self, t):
        def tvsT(t):
            try:
                self.plotting_parameters.child("Mass spec")
            except AttributeError:
                print("no curves yet")
            else:
                if t:
                    self.X = "Time [min]"
                    self.tgms_plot.plot.getAxis("bottom").setLabel(text="Time", units='min')
                    print("plotting vs. time")
                else:
                    self.X = "Temperature [°C]"
                    self.tgms_plot.plot.getAxis("bottom").setLabel(text="Temperature", units='°C')
                    print("plotting vs. T°")
                self.update_tgms_plot()
        return tvsT

    def make_log(self, islog):
        def log(islog):
            try:
                self.plotting_parameters.child("Mass spec")
            except AttributeError:
                print("no curves yet")
            else:
                if islog:
                    self.tgms_plot.plot.setLogMode(False, True)
                    # self.update_tgms_plot()
                    print("switched to log")
                else:
                    self.tgms_plot.plot.setLogMode(False, False)
                    # self.update_tgms_plot()
                    print("switched to linear")
        return log
    
    def open(self):        
        filename, filter = QtWidgets.QFileDialog.getOpenFileName(caption="Open", filter="json files (*.json);;All files(*)")
        with open(filename, "r") as handle:
            loaded_dict = json.load(handle)
        
        self.experimental_parameters.restoreState(loaded_dict["experimental_parameters"])
        self.tgms = pickle.loads(base64.b64decode(loaded_dict["tgms"]))
        self.generate_plotting_parameters()

    def save(self):
        to_json = {
            "experimental_parameters": self.experimental_parameters.saveState(),
            "tgms": base64.b64encode(pickle.dumps(self.tgms)).decode("utf8")
        }
        filename, filter = (QtWidgets.QFileDialog.getSaveFileName(caption="Save File", filter="json files (*.json);;All files(*)"))
        with open(filename, "w") as handle:
            json.dump(to_json, handle)

    def settings(self):
        print("opening settings window")
        settings_window = settings_dialog(self)

    def export(self):
        print("Export data")
        filename, filter = (QtWidgets.QFileDialog.getSaveFileName(caption="Export as CSV", filter="csv files (*.csv);;All files(*)"))
        self.tgms.export_csv(filename)
        
    def about(self):
        box = QtWidgets.QMessageBox()
        box.setText(f"I'm a data processor & plotter\nfor TGMS experiments\n@ Djamila's lab.\n\nI'm meant to make things easy.\n\nIf you're not satisfied with me,*\ncall Sylvain!\n\n\n* I'm still at version {VERSION}\nso don't be too harsh!")
        box.setWindowTitle("About")
        box.exec()


class PandasModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._data = data

    def rowCount(self, parent=None):
        return len(self._data.values)

    def columnCount(self, parent=None):
        return self._data.columns.size

    def data(self, index, role=QtCore.Qt.ItemDataRole.DisplayRole):
        if index.isValid():
            if role == QtCore.Qt.ItemDataRole.DisplayRole:
                return QtCore.QVariant(str(
                    self._data.iloc[index.row()][index.column()]))
        return QtCore.QVariant()
    
    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Orientation.Horizontal and role == QtCore.Qt.ItemDataRole.DisplayRole:
            return self._data.columns[col]
        return None


class TGMSplot(pg.GraphicsLayoutWidget):
        
    def __init__(self):
        super().__init__()

        self.ui_setup()

    def ui_setup(self):

        # style
        self.setBackground(None)
        self.palette = color_palette("tab10")
        self.palette2 = color_palette("pastel")
        
        self.pens = []
        for color in self.palette.as_hex():
            self.pens.append(pg.mkPen(color=pg.mkColor(color), width=2))
        for color in self.palette2.as_hex():
            self.pens.append(pg.mkPen(color=pg.mkColor(color), width=2))
        self.pens2 = []
        for color in self.palette2.as_hex():
            self.pens2.append(pg.mkPen(color=pg.mkColor(color), width=2))
        for color in self.palette2.as_hex():
            self.pens2.append(pg.mkPen(color=pg.mkColor(color), width=2))
        
        # # plot item
        # self.layout = pg.GraphicsLayout()
        self.plot = self.addPlot()
        self.plot.getAxis("bottom").enableAutoSIPrefix(enable=False)
        self.plot.getAxis("bottom").setLabel(text="Temperature", units='°C')
        self.plot.getAxis("left").enableAutoSIPrefix(enable=False)
        self.plot.getAxis("left").setLabel(text="Mass Spec Signal", units="bar", unitPrefix="m")
        self.legend = self.plot.addLegend()
        self.global_viewbox = self.plot.vb

        # viewboxes
        self.tg_viewbox = pg.ViewBox()
        self.tg_viewbox.setXLink(self.global_viewbox)

        # axes
        self.mass_axis = pg.AxisItem("right")
        self.mass_axis.setLabel("Sample Mass", units='%')
        self.mass_axis.enableAutoSIPrefix(enable=False)

        # compose layout
        self.addItem(self.plot)
        self.addItem(self.mass_axis)
        self.plot.scene().addItem(self.tg_viewbox)
        self.mass_axis.linkToView(self.tg_viewbox)

        # context menu (renaming axes)
        rename_axesx_action = QtWidgets.QWidgetAction(self)
        rename_axesx_layout = QtWidgets.QHBoxLayout()
        rename_axesx_layout.addWidget(QtWidgets.QLabel("Rename"))
        self.rename_axesx_edit = QtWidgets.QLineEdit(text=self.plot.getAxis("bottom").label.toPlainText())
        rename_axesx_layout.addWidget(self.rename_axesx_edit)
        rename_axesx_widget = QtWidgets.QWidget()
        rename_axesx_widget.setLayout(rename_axesx_layout)
        rename_axesx_action.setDefaultWidget(rename_axesx_widget)

        rename_axesy_action = QtWidgets.QWidgetAction(self)
        rename_axesy_layout = QtWidgets.QHBoxLayout()
        rename_axesy_layout.addWidget(QtWidgets.QLabel("Rename"))
        self.rename_axesy_edit = QtWidgets.QLineEdit(text=self.plot.getAxis("left").label.toPlainText())
        rename_axesy_layout.addWidget(self.rename_axesy_edit)
        rename_axesy_widget = QtWidgets.QWidget()
        rename_axesy_widget.setLayout(rename_axesy_layout)
        rename_axesy_action.setDefaultWidget(rename_axesy_widget)

        rename_axesr_action = QtWidgets.QWidgetAction(self)
        rename_axesr_layout = QtWidgets.QHBoxLayout()
        rename_axesr_layout.addWidget(QtWidgets.QLabel("Rename"))
        self.rename_axesr_edit = QtWidgets.QLineEdit(text=self.mass_axis.label.toPlainText())
        rename_axesr_layout.addWidget(self.rename_axesr_edit)
        rename_axesr_widget = QtWidgets.QWidget()
        rename_axesr_widget.setLayout(rename_axesr_layout)
        rename_axesr_action.setDefaultWidget(rename_axesr_widget)

        # updating axes labels
        self.rename_axesx_edit.textChanged.connect(partial(self.rename_axis, "bottom"))
        self.rename_axesy_edit.textChanged.connect(partial(self.rename_axis, "left"))
        self.rename_axesr_edit.textChanged.connect(partial(self.rename_axis, "right"))

        self.global_menu = self.global_viewbox.menu
        self.global_menu.axes[0].addAction(rename_axesx_action)
        self.global_menu.axes[1].addAction(rename_axesy_action)

        self.tg_menu = self.tg_viewbox.menu
        self.tg_menu.axes[1].addAction(rename_axesr_action)
        self.tg_menu.ctrlMenu = None
        self.tg_menu.axes[0] = None 

    def rename_axis(self, type):
        if type == "bottom":
            self.plot.getAxis("bottom").setLabel(self.rename_axesx_edit.text())
        if type == "left":
            self.plot.getAxis("left").setLabel(self.rename_axesy_edit.text())
        elif type == "right":
            self.mass_axis.setLabel(self.rename_axesr_edit.text())

    def add_ms_plot(self, datax, datay, name):
        item = pg.PlotDataItem(
                datax,
                datay,
                pen=self.pens[
                    self.count_ms_curves()%len(self.pens)
                ],
                name=name
            )
        self.plot.addItem(item)
        # self.plot.legend.addItem(item, name=name)

    def add_tg_plot(self, datax, datay, name):
        item = pg.PlotDataItem(
                datax,
                datay,
                pen=self.pens2[
                    self.count_tg_curves()%len(self.pens2)
                ],
                name=name
            )
        self.tg_viewbox.addItem(item)
        self.tg_viewbox.setGeometry(self.plot.getViewBox().sceneBoundingRect())
        self.legend.addItem(item, name=name)

    def count_ms_curves(self):
        return len(self.global_viewbox.allChildren())//3
    
    def count_tg_curves(self):
        return len(self.tg_viewbox.allChildren())//3

    def clf(self):
        print("cleared!")
        self.plot.clear()
        self.global_viewbox.clear()
        self.tg_viewbox.clear()
        self.legend.clear()


class LineNumberArea(QtWidgets.QWidget):
    def __init__(self, editor):
        super().__init__(editor)
        self.editor = editor

    def sizeHint(self):
        return QtCore.QSize(self.editor.lineNumberAreaWidth(), 0)

    def paintEvent(self, event):
        print('LineNumberArea.paintEvent')
        self.editor.lineNumberAreaPaintEvent(event)


class TextPreviewWidget(QtWidgets.QPlainTextEdit):
    def __init__(self):
        super().__init__()
        self.lineNumberArea = LineNumberArea(self)

        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)

        self.updateLineNumberAreaWidth(0)

        self.setReadOnly(True)
        self.setFont(QtGui.QFont("Consolas"))

    def lineNumberAreaWidth(self):
        """ This method has been slightly modified (use of log and uses actual
        font rather than standart.) """
        n_lines = self.blockCount()
        digits = np.ceil(np.log10(n_lines)) + 1
        return int(digits * QtGui.QFontMetrics(self.font()).averageCharWidth() + 10)

    def updateLineNumberAreaWidth(self, _):
        # print('CodeEditor.updateLineNumberAreaWidth: margin = {}'.format(self.lineNumberAreaWidth()))
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)

    def updateLineNumberArea(self, rect, dy):
        # print('CodeEditor.updateLineNumberArea: rect = {}, dy = {}'.format(rect, dy))

        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(),
                                       rect.height())

        # print('CodeEditor.updateLineNumberArea: rect.contains(self.viewport().rect()) = {}'.format(rect.contains(self.viewport().rect())))
        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)

    def resizeEvent(self, event):
        super().resizeEvent(event)

        cr = self.contentsRect();
        self.lineNumberArea.setGeometry(QtCore.QRect(cr.left(), cr.top(),
                                        self.lineNumberAreaWidth(), cr.height()))

    def lineNumberAreaPaintEvent(self, event):
        # print('CodeEditor.lineNumberAreaPaintEvent')
        painter = QtGui.QPainter(self.lineNumberArea)
        # painter.fillRect(event.rect(), QtCore.Qt.GlobalColor.lightGray)

        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
        bottom = top + self.blockBoundingRect(block).height()

        margin = 10

        # Just to make sure I use the right font
        height = QtGui.QFontMetrics(self.font()).height()
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNumber + 1)
                painter.setPen(QtCore.Qt.GlobalColor.gray)
                painter.setFont(QtGui.QFont("consolas"))
                painter.drawText(0, int(top), self.lineNumberArea.width() - margin, height,
                             QtCore.Qt.AlignmentFlag.AlignRight, number)

            block = block.next()
            top = bottom
            bottom = top + self.blockBoundingRect(block).height()
            blockNumber += 1


    def highlightCurrentLine(self):
        extraSelections = []
        selection = QtWidgets.QTextEdit.ExtraSelection()

        QtWidgets.QPushButton.palette
        lineColor = QtGui.QColor(128, 128, 128, 85)

        selection.format.setBackground(lineColor)
        selection.format.setProperty(QtGui.QTextFormat.Property.FullWidthSelection, True)
        selection.cursor = self.textCursor()
        selection.cursor.clearSelection()
        extraSelections.append(selection)
        self.setExtraSelections(extraSelections)

class Experimental_parameters():
        
    def __init__(self):
        self.dict = {
            "sample_nature": {
                "name": "Sample nature",
                "value": ""
            },
            "sample_mass": {
                "name": "Sample mass",
                "value": 0*pq.milligram,
            },
            "crucible_mass": {
                "name": "Crucible mass",
                "value": 0*pq.milligram,
            },
            "atmosphere": {
                "name": "Atmosphere",
                "value": "",
            },
            "flow_purge1": {
                "name": "Flow purge 1",
                "value": 0*pq.millilitre/pq.minute,
            },
            "flow_purge2": {
                "name": "Flow purge 2",
                "value": 0*pq.millilitre/pq.minute,
            },
            "flow_protective": {
                "name": "Flow protective",
                "value": 0*pq.millilitre/pq.minute,
            },
            "max_temp": {
                "name": "Max temperature",
                "value": 0*pq.celsius,
            },
            "temp_rise": {
                "name": "Temperature rise",
                "value": 0*pq.celsius/pq.minute,
            },
            "temp_plateau": {
                "name": "Temperature plateau",
                "value": 0*pq.minute,
            },
            # "commentary" : {
            #     "name": "Commentary",
            #     "value": "",
            # }
        }

def main():

    
    app = QtWidgets.QApplication(sys.argv)
    pg.setConfigOptions(antialias=True)
    qdarktheme.enable_hi_dpi()
    win = Window()

    try:
        with open(SETTINGS_FILE, "r") as handle:
            if json.load(handle)["Appearance"]["Dark mode"]:
                qdarktheme.setup_theme("dark")
            else:
                qdarktheme.setup_theme("light")
    except FileNotFoundError:
            qdarktheme.setup_theme("auto")

    win.show()
    timer = QtCore.QTimer()
    timer.timeout.connect(win.update)
    timer.start(50)
    sys.exit(app.exec())


if __name__=="__main__":
    main()