from numpy import interp
import pandas as pd
import quantities as pq
import codecs
from io import StringIO

class TGMS_data:

    def __init__(self):
        pass
        # self.import_ms_data("C:/Users/adminLocal/Documents/_zet/djamila/cac2o4_he_10k_51mg_50_41.csv", rows_to_skip=31)
        # self.import_tg_data("C:/Users/adminLocal/Documents/_zet/djamila/ExpDat_CaC2O451mgHe10K5040_copy.csv", rows_to_skip=33)
        # self.interp_temperature()

    def check_for_ms(self):
        try:
            print(self.ms_channels)
        except AttributeError:
            print("MS data not imported yet!")
            return 0
        else:
            return 1
    
    def check_for_tg(self):
        try:
            print(self.tg_channels)
        except AttributeError:
            print("TG data not imported yet!")
            return 0
        else:
            return 1

    def add_parameter(self, key, name, type, value):
        self.default_experiment_parameters[key] = {
            "name": name,
            "type": type,
            "value": value
        }

    def drop_parameter(self, key):
        self.default_experiment_parameters.pop(key)

    def import_ms_data(self, content, rows_to_skip, sep, decimal):
        df = pd.read_csv(
            StringIO(content),
            sep=sep,
            skipinitialspace=True,
            decimal=decimal,
            skiprows=rows_to_skip)
        try:
            df.insert(0, "Time [s]", df["ms"]/1e3)
        except KeyError:
            df.insert(0, "Time [s]", df["RelTime[s]"])
        df.insert(0, "Time [min]", df["Time [s]"]/60)

        self.ms_data = df
        self.ms_channels = list(df)
    
    def import_tg_data(self, content, rows_to_skip, sep, decimal):
        # BLOCKSIZE = 1048576 # or some other, desired size in bytes
        # with codecs.open(path, "r", "ansi") as sourceFile:
        #     with codecs.open(path+"utf8", "w", "utf-8") as targetFile:
        #         while True:
        #             contents = sourceFile.read(BLOCKSIZE)
        #             if not contents:
        #                 break
        #             targetFile.write(contents)

        df = pd.read_csv(
            StringIO(content),
            sep=sep,
            skipinitialspace=True,
            decimal=decimal,
            skiprows=rows_to_skip)
        
        rename_dict = {
            "##Temp./â€ºC":"Temperature [°C]",
            "##Temp./›C":"Temperature [°C]",
            "##Temp./C":"Temperature [°C]",
            "Time/min":"Time [min]",
            "Mass/%":"Mass [%]",
            "Gas Flow(purge2)/(mL/min)":"Gas Flow (purge2) [ml/min]",
            "Gas Flow(protective)/(mL/min)":"Gas Flow (protective) [ml/min]"
        }

        df = df.rename(columns=rename_dict)

        df["dMass/dT"] = (df["Mass [%]"].diff().ewm(span = 50).mean()/df["Temperature [°C]"])
        
        self.tg_data = df
        self.tg_channels = list(df)

    def interp_temperature(self):
        # try:
        #     value = self.tg_data
        # except AttributeError:
        #     pass
        # try:
        #     value = self.ms_data
        # except AttributeError:
        #     pass
        self.ms_data["Temperature [°C]"] = interp(
            self.ms_data["Time [min]"],
            self.tg_data["Time [min]"],
            self.tg_data["Temperature [°C]"]
        )

    def export_csv(self, filename):
        export_dataframe = self.tg_data
        for channel in self.ms_channels:
            export_dataframe[channel] = interp(
                export_dataframe["Time [min]"],
                self.ms_data["Time [min]"],
                self.ms_data[channel]
            )
        return export_dataframe.to_csv(filename)

if __name__=="__main__":
    pass